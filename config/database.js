const Sequelize = require('sequelize');
const mysql = require('mysql2');

var sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
  host: process.env.DB_HOST,
  dialect: 'mysql',
  timezone: process.env.TZ,
});

var native = mysql.createConnection({
  host : process.env.DB_HOST,
  user : process.env.DB_USERNAME,
  password : process.env.DB_PASSWORD,
  database : process.env.DB_NAME
});

const doQuery = async(query, data) => {
  return new Promise((resolve, reject) => {
      native.query(query, data, (err, rows, fields) => {
          if (err) reject(err);
          else resolve(rows);
      });
  });
}

module.exports = {
  sequelize: sequelize,
  native: native,
  doQuery: doQuery,
};