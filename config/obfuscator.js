const javascriptObfuscator = require('javascript-obfuscator');
const fs = require('fs');
const path = require('path');

const inputDir = path.join(__dirname, '../resources/javascripts');
const outputDir = path.join(__dirname, '../public/javascripts');

var _fileCollection = [];

var option = {
    compact: true,
    controlFlowFlattening: false,
    controlFlowFlatteningThreshold: 0.75,
    deadCodeInjection: true,
    deadCodeInjectionThreshold: 0.4,
    debugProtection: true,
    debugProtectionInterval: false,
    disableConsoleOutput: true,
    domainLock: [],
    identifierNamesGenerator: 'hexadecimal',
    identifiersDictionary: [],
    identifiersPrefix: '',
    inputFileName: '',
    log: false,
    renameGlobals: true,
    reservedNames: [],
    reservedStrings: [],
    rotateStringArray: true,
    seed: 0,
    selfDefending: true,
    shuffleStringArray: true,
    sourceMap: false,
    sourceMapBaseUrl: '',
    sourceMapFileName: '',
    sourceMapMode: 'separate',
    splitStrings: true,
    splitStringsChunkLength: 10,
    stringArray: true,
    stringArrayEncoding: "base64",
    stringArrayThreshold: 0.8,
    target: 'browser',
    transformObjectKeys: true,
    unicodeEscapeSequence: false
};

var getAllFiles = () => {
    return new Promise((resolve, reject) => {
        fs.readdir(inputDir, function (err, filenames) {
            if (err) {
                reject( err );
            }
            resolve( filenames );
        });
    });
}

var readFiles = (filename) => {
    return new Promise((resolve, reject) => {
        fs.readFile(path.join(inputDir, filename), 'utf-8', function (err, content) {
            if (err) {
                reject( err );
            }
            resolve( content );
        });
    });
}

var obfuscateAllFiles = () => {
    return new Promise((resolve, reject) => {
        _fileCollection.forEach(function (filename) {
            readFiles(filename).then((data) => {
                fs.writeFileSync(path.join(outputDir, filename), javascriptObfuscator.obfuscate(data), option);
                console.log(`File ${filename} obfuscated`);
            }).catch((err) => {
                console.log(err);
            });
        });
        resolve( "Done" );
    });
}

getAllFiles().then(async (data) => {
    _fileCollection = data;
    console.log(_fileCollection);
    obfuscateAllFiles();
}).catch((err) => {
    console.log(err);
});