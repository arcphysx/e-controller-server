'use strict';

const SHA256 = require("crypto-js/sha256")

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('device', [{
      id_user: 1,
      name: "NodeMCU 32S",
      key: SHA256('1' + (new Date()).toString() + process.env.APP_KEY).toString().substr(0, 32),
      socket_uid: null,
      created_at: new Date(),
      created_by: 1,
      updated_at: new Date(),
      updated_by: 1,
    }]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('device', null, {});
  }
};
