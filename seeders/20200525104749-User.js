'use strict';

const bcrypt = require('bcrypt');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('user', [{
      username: 'arcphysx',
      password: bcrypt.hashSync("abc123", 10),
      name: 'Raymond',
      last_login: new Date(),
      created_at: new Date(),
      created_by: 0,
      updated_at: new Date(),
      updated_by: 0,
    }]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user', null, {});
  }
};
