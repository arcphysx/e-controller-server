'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('component', [{
      id_device: 1,
      name: "Lampu LED RGB",
      type: "led_rgb",
      value: `{
        "pin": [25, 26, 27],
          "color": [0, 0, 0]
      }`,
      created_at: new Date(),
      created_by: 1,
      updated_at: new Date(),
      updated_by: 1,
    },
    {
      id_device: 1,
      name: "Lampu LED",
      type: "led",
      value: `{
        "pin": [12],
          "state": 0
      }`,
      created_at: new Date(),
      created_by: 1,
      updated_at: new Date(),
      updated_by: 1,
    },
    {
      id_device: 1,
      name: "Sensor Suhu",
      type: "sensor_temperature",
      value: `{
        "pin": [ 32 ],
        "interval": 200,
        "humidity": 77,
        "temperature": 31
      }`,
      created_at: new Date(),
      created_by: 1,
      updated_at: new Date(),
      updated_by: 1,
    }]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('component', null, {});
  }
};
