'use strict';
const Sequelize = require('sequelize');
const db = require('../config/database');
const models = require('require-all')(__dirname);

const Component = db.sequelize.define('Component', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: Sequelize.INTEGER(11).UNSIGNED
  },
  id_device: Sequelize.INTEGER(11).UNSIGNED,
  name: Sequelize.STRING,
  type: Sequelize.STRING,
  value: Sequelize.TEXT,
}, {
  paranoid: true,
  timestamps: true,
  underscored: true,
  freezeTableName: true,
  tableName: 'component',
});

Component.belongsTo(models.Device, {targetKey: 'id', foreignKey: 'id_device', as: 'Device'});
models.Device.hasMany(Component, {foreignKey: 'id_device', as: 'Component'});

module.exports = Component;