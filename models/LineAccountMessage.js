'use strict';
const Sequelize = require('sequelize');
const db = require('../config/database');
const models = require('require-all')(__dirname);

const LineAccountMessage = db.sequelize.define('LineAccountMessage', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: Sequelize.INTEGER(11).UNSIGNED
  },
  id_line_account: Sequelize.INTEGER(11).UNSIGNED,
  id_inbound_dump: Sequelize.INTEGER(11).UNSIGNED,
  type: Sequelize.STRING,
  message_id: Sequelize.STRING,
  content: Sequelize.TEXT,
}, {
  paranoid: true,
  timestamps: true,
  underscored: true,
  freezeTableName: true,
  tableName: 'line_account_message',
});

module.exports = LineAccountMessage;