'use strict';
const Sequelize = require('sequelize');
const db = require('../config/database');
const models = require('require-all')(__dirname);
const axios = require('axios');

const LineAccount = db.sequelize.define('LineAccount', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: Sequelize.INTEGER(11).UNSIGNED
  },
  id_user: Sequelize.INTEGER(11).UNSIGNED,
  line_user_id: Sequelize.STRING,
  display_name: Sequelize.STRING,
  status: Sequelize.STRING,
  picture_url: Sequelize.STRING,
  language: Sequelize.STRING,
}, {
  paranoid: true,
  timestamps: true,
  underscored: true,
  freezeTableName: true,
  tableName: 'line_account',
});

LineAccount.belongsTo(models.User, {targetKey: 'id', foreignKey: 'id_user', as: 'User'});
models.User.hasMany(LineAccount, {foreignKey: 'id_user', as: 'LineAccount'});

models.LineAccountMessage.belongsTo(LineAccount, {targetKey: 'id', foreignKey: 'id_line_account', as: 'LineAccount'});
LineAccount.hasMany(models.LineAccountMessage, {foreignKey: 'id_line_account', as: 'LineAccountMessage'});

LineAccount.prototype.syncDataWithLineServer = async function () {
  var config = {
    headers: {
        'Authorization' : 'Bearer ' + process.env.LINE_API_KEY,
        'Content-Type' : 'application/json',
    }
  };
  axios.get("https://api.line.me/v2/bot/profile/" + this.line_user_id, config)
  .then(async (response) => {
    this.display_name = (typeof response.data.displayName != 'undefined' ? response.data.displayName : "");
    this.picture_url = (typeof response.data.pictureUrl != 'undefined' ? response.data.pictureUrl : "");
    this.status = (typeof response.data.statusMessage != 'undefined' ? response.data.statusMessage : "");
    this.language = (typeof response.data.language != 'undefined' ? response.data.language : "");
    await this.save();
  }).catch((error) => {
    console.log(error);
  });
}
module.exports = LineAccount;