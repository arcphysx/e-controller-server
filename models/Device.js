'use strict';
const Sequelize = require('sequelize');
const db = require('../config/database');
const models = require('require-all')(__dirname);

const Device = db.sequelize.define('Device', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: Sequelize.INTEGER(11).UNSIGNED
  },
  id_user: Sequelize.INTEGER(11).UNSIGNED,
  name: Sequelize.STRING,
  key: Sequelize.STRING,
  socket_uid: Sequelize.STRING,
}, {
  paranoid: true,
  timestamps: true,
  underscored: true,
  freezeTableName: true,
  tableName: 'device',
});

Device.belongsTo(models.User, {targetKey: 'id', foreignKey: 'id_user', as: 'User'});
models.User.hasMany(Device, {foreignKey: 'id_user', as: 'Device'});

module.exports = Device;