// ---------------------------------------------------------------
$(document).ready(function () {
    var _back_validate = () => {$.ajax({
        url:'/backend/check',
        headers: { 'authorization': localStorage.getItem("x_auth_token") },
        data: {},
        type: 'POST',
        dataType: 'html',
        beforeSend: function() {
            //
        },
        complete:function() {
            //
        },
        success:function(data) {
            toastr.success(`Last successful login:<br/>${last_dt.getFullYear()}-${(last_dt.getMonth()+1)}-${last_dt.getDate()} ${last_dt.getHours()}:${last_dt.getMinutes()}:${last_dt.getSeconds()}`);
        },
        error: function(data){
            localStorage.removeItem("x_auth_token");
            window.location.href = "/";
        }
    })};
    _back_validate();
});

$(document).ready(function () {
    M.Sidenav.init($('.sidenav'));
    $('.btn-about').click(() => {
        Swal.fire({
            title: 'About This Project',
            html:`
                <strong>E-Controller</strong> is an <i>Embedded System Project</i> 
                <br/>
                created by <strong>Raymond A P</strong>.
                <br/><br/>
                This app is useful for automate your daily electronic device with microcontroller, moreover you can connect your LINE Account with this app.
                <br/><br/>
                <strong>&copy;${(new Date()).getFullYear()} Raymond A P</strong>
            `,
            width: 600,
            padding: '3em',
            background: 'repeating-linear-gradient(43deg, rgba(161,159,159, 0.15) 0px, rgba(161,159,159, 0.15) 1px,transparent 1px, transparent 41px),repeating-linear-gradient(133deg, rgba(161,159,159, 0.15) 0px, rgba(161,159,159, 0.15) 1px,transparent 1px, transparent 41px),linear-gradient(133deg, rgb(179,229,252),rgb(179,229,252))',
            backdrop: `
              #82b1ff66
              url("/images/nyan-cat.gif")
              left top
              no-repeat
            `,
            imageUrl: '/images/logo.webp',
            imageWidth: 128,
            imageHeight: 128,
            imageAlt: 'E Controller Logo',
        });
    });
});

// ---------------------------------------------------------------

var _get_line_data;
$(document).ready(function () {
    _get_line_data = () => {$.ajax({
        url:'/line/connected',
        headers: { 'authorization': localStorage.getItem("x_auth_token") },
        data: {},
        type: 'GET',
        dataType: 'html',
        beforeSend: function() {
            $('#user-load .progress').fadeIn();
        },
        complete:function() {
            $('#user-load .progress').fadeOut();							
        },
        success:function(data) {
            data = JSON.parse(data);
            // console.log(data);
            if(data.data != null){
                $('#line-name').html(data.data.display_name);
                $('#line-status').html(data.data.status);
                $('#line-language').html(data.data.language);
                $('#line-photo').attr('src', data.data.picture_url);
                $('#tabLine .btn-logout').show();
            }else{
                $('#line-name').html("N/A");
                $('#line-status').html("N/A");
                $('#line-language').html("N/A");
                $('#line-photo').attr('src', "/images/logo.webp");
                $('#tabLine .btn-logout').hide();
            }
        },
        error: function(data){
            data = JSON.parse(data.responseText);
            toastr.error(`[Load Line] ${data.message}`);
        }
    })};
    _get_line_data();

    $('#tabLine .btn-logout').click(() => {
        socket.emit('line_logout', {
            "user_id": user.id,
        });
    });
});

// ---------------------------------------------------------------

var _get_device_list;
var _get_component_list;
$(document).ready(function () {
    _get_device_list = () => {$.ajax({
        url:'/backend/devices',
        headers: { 'authorization': localStorage.getItem("x_auth_token") },
        data: {},
        type: 'GET',
        dataType: 'html',
        beforeSend: function() {
            $('#node-list .progress').fadeIn();
        },
        complete:function() {
            $('#node-list .progress').fadeOut();							
        },
        success:function(data) {
            $('#node-list .collection').empty();
            $('#node-list .collection').append(data);
        },
        error: function(data){
            data = JSON.parse(data.responseText);
            toastr.error(`[Load Device] ${data.message}`);
        }
    })};
    _get_device_list();


    _get_component_list = (deviceId) => {$.ajax({
        url:`/backend/devices/${deviceId}/components`,
        headers: { 'authorization': localStorage.getItem("x_auth_token") },
        data: {},
        type: 'GET',
        dataType: 'html',
        beforeSend: function() {
            $('#component-list .progress').fadeIn();
        },
        complete:function() {
            $('#component-list .progress').fadeOut();							
        },
        success:function(data) {
            $('#component-list .component-container').empty();
            $('#component-list .component-container').append(data);
            initJSONTextArea();
            M.updateTextFields();
            M.FormSelect.init($('select'));
        },
        error: function(data){
            data = JSON.parse(data.responseText);
            toastr.error(`[Load Device] ${data.message}`);
        }
    })};

    $(document).on('click', '#node-list .collection-item', (obj) => {
        $('#component-list p').html("Selected Device: " + obj.currentTarget.getAttribute('data-name'));
        $('#component-list .btn-add').attr('data-id', obj.currentTarget.getAttribute('data-id'));
        $('#component-list .btn-add').attr('disabled', false);

        $('#node-list .btn-edit').attr('data-id', obj.currentTarget.getAttribute('data-id'));
        $('#node-list .btn-edit').attr('disabled', false);
        $('#node-list .btn-edit').click(() => {
            $('#modalEditNode input[name=name]').val(obj.currentTarget.getAttribute('data-name'));
            M.updateTextFields();
        });
        $('#node-list .btn-delete').attr('data-id', obj.currentTarget.getAttribute('data-id'));
        $('#node-list .btn-delete').attr('disabled', false);

        _get_component_list(obj.currentTarget.getAttribute('data-id'));
    });
});

// ---------------------------------------------------------------

var initJSONTextArea = () => {
    var jText = $('.json-textarea');
    for(let i=0; i<jText.length; i++){
        var editor = CodeMirror.fromTextArea(jText[i], {
            lineNumbers: true,
            theme:"material",
            mode: "application/json",
        });

        editor.needEmitChanges = true;

        var valueTimeout;
        editor.on('change', (e) => {
            jText[i].value = e.getValue();
            if(e.needEmitChanges == true){
                // console.log("My State: " + editor.needEmitChanges)
                clearTimeout(valueTimeout);
                valueTimeout = setTimeout(()=>{
                    // console.log({
                    //     "id": $(jText[i]).parents('.component-col')[0].getAttribute('data-id'),
                    //     "value":e.getValue()
                    // });
                    socket.emit('component_update', {
                        "id": $(jText[i]).parents('.component-col')[0].getAttribute('data-id'),
                        "value":e.getValue()
                    });
                    console.log('Changes Emitted!');
                }, 500);
            }
        });
        jsonTextAreas[$(jText[i]).parents('.component-col')[0].getAttribute('id')] = editor;
    }
}

// ---------------------------------------------------------------

$(document).ready(function () {
    var titleTimeout;
    $(document).on('input', '.component-col .component-title', (obj) => {
        clearTimeout(titleTimeout);
        titleTimeout = setTimeout(()=>{
            // console.log({
            //     "id": $(obj.currentTarget).parents('.component-col')[0].getAttribute('data-id'),
            //     "name":obj.currentTarget.value
            // });
            socket.emit('component_update', {
                "id": $(obj.currentTarget).parents('.component-col')[0].getAttribute('data-id'),
                "name":obj.currentTarget.value
            });
            console.log('Changes Emitted!');
        }, 500);
    });

    $(document).on('change', '.component-col .component-select', (obj) => {
        // console.log({
        //     "id": $(obj.currentTarget).parents('.component-col')[0].getAttribute('data-id'),
        //     "type":obj.currentTarget.value
        // });
        socket.emit('component_update', {
            "id": $(obj.currentTarget).parents('.component-col')[0].getAttribute('data-id'),
            "type":obj.currentTarget.value
        });
        console.log('Changes Emitted!');
    });
});

// ---------------------------------------------------------------

$(document).ready(function () {
    $(document).on('click', '#component-list .btn-add', (obj) => {
        socket.emit('component_add', {
            "user_id": user.id,
            "device_id": $(obj.currentTarget).data('id'),
        });
    });

    $(document).on('click', '#component-list .btn-delete', (obj) => {
        // console.log({
        //     "user_id": user.id,
        //     "comp_id": $(obj.currentTarget).parents('.component-col')[0].getAttribute('data-id'),
        // });
        socket.emit('component_delete', {
            "user_id": user.id,
            "comp_id": $(obj.currentTarget).parents('.component-col')[0].getAttribute('data-id'),
        });
    });
});

// ---------------------------------------------------------------

$(document).ready(function () {
    $('#modalAddNode button').click(() => {
        socket.emit('device_add', {
            "user_id": user.id,
            "name": $('#modalAddNode input[name=name]').val()
        });
    });

    $('#modalEditNode button').click(() => {
        socket.emit('device_edit', {
            "device_id": $('#node-list .btn-edit').attr('data-id'),
            "user_id": user.id,
            "name": $('#modalEditNode input[name=name]').val()
        });
    });

    $('#modalDeleteNode button').click(() => {
        socket.emit('device_delete', {
            "device_id": $('#node-list .btn-edit').attr('data-id'),
            "user_id": user.id,
        });
    });

});

// ---------------------------------------------------------------

var nodeRefresh;
function refreshNodeStatusInterval(){
    nodeRefresh = setInterval(() => {
        socket.emit('panel_req', {req: 'node_status'});
    }, 3000);
}

function clearNodeStatusInterval(){
    clearInterval(nodeRefresh);
}

refreshNodeStatusInterval();

// ---------------------------------------------------------------

socket.on('connect_failed', function(){
    $('.srv-status').hide(()=>{ $('#srv-error').show(); $('#srv-error pre').html('Connection Failed'); });
    $('#srv-load').show();
});

socket.on('connect', function(){
    $('.srv-status').hide(()=>{ $('#srv-connected').show(); $('#srv-connected pre').html('Connection Established'); });
    $('#srv-load').hide();
});

socket.on('disconnect', function () {
    $('.srv-status').hide(()=>{ $('#srv-disconnected').show(); $('#srv-disconnected pre').html('Connection Lost'); });
    $('#srv-load').show();
    $('#node-list .collection .collection-item p').html('Offline');
    $('#node-list .collection .collection-item p').css('color', '#37474f');
});

socket.on('error', function(err) {
    $('.srv-status').hide(()=>{ $('#srv-error').show(); $('#srv-error pre').html(err.message); });
    $('#srv-load').show();
});

socket.on('test_ping', function(data) {
    console.log("PING Received: ");
    console.log(data);
});

socket.on('device_join', (data) => {
    // console.log("join");
    // console.log(data);
    $(`#node_list_${data.key} p`).html('Online');
    $(`#node_list_${data.key} p`).css('color', '#4caf50');
    M.toast({html: `${data.name} is now online`, classes: 'rounded'});
});

socket.on('device_leave', (data) => {
    // console.log("leave");
    // console.log(data);
    $(`#node_list_${data.key} p`).html('Offline');
    $(`#node_list_${data.key} p`).css('color', '#37474f');
    M.toast({html: `${data.name} is now offline`, classes: 'rounded'});
});

socket.on('component_add_callback', (data) => {
    // console.log(data);
    if(data.code >= 200 && data.code < 300){
        // console.log(`node_list_${data.data.device_key}`);
        $(`#node_list_${data.data.device_key}`).click();
    }else{
         M.toast({html: "Oops... something went wrong", classes: 'rounded'});
    }
});

socket.on('component_delete_callback', (data) => {
    // console.log(data);
    if(data.code >= 200 && data.code < 300){
        // console.log(`node_list_${data.data.device_key}`);
        $(`#node_list_${data.data.device_key}`).click();
    }else{
         M.toast({html: "Oops... something went wrong", classes: 'rounded'});
    }
});

socket.on('device_add_callback', (data) => {
    // console.log(data);
    if(data.code >= 200 && data.code < 300){
        $('#modalAddNode input[name=name]').val("");
        M.Modal.getInstance($('#modalAddNode')).close();
        _get_device_list();
    }else{
         M.toast({html: "Oops... something went wrong", classes: 'rounded'});
    }
});

socket.on('device_edit_callback', (data) => {
    // console.log(data);
    if(data.code >= 200 && data.code < 300){
        $('#modalEditNode input[name=name]').val(data.data.name);
        M.Modal.getInstance($('#modalEditNode')).close();
        $('#component-list p').html("Selected Device: None");
        $('#component-list .component-container').empty();
        $('#component-list .btn-add').attr('disabled', true);
        _get_device_list();
    }else{
         M.toast({html: "Oops... something went wrong", classes: 'rounded'});
    }
});

socket.on('device_delete_callback', (data) => {
    // console.log(data);
    if(data.code >= 200 && data.code < 300){
        M.Modal.getInstance($('#modalDeleteNode')).close();
        $('#component-list p').html("Selected Device: None");
        $('#component-list .component-container').empty();
        $('#component-list .btn-add').attr('disabled', true);
        $('#node-list .btn-edit').attr('disabled', true);
        $('#node-list .btn-delete').attr('disabled', true);
        _get_device_list();
    }else{
         M.toast({html: "Oops... something went wrong", classes: 'rounded'});
    }
});

socket.on('component_changes_callback', (data) => {
    if(typeof jsonTextAreas[`comp_list_${data.id}`] !== 'undefined'){
        jsonTextAreas[`comp_list_${data.id}`].needEmitChanges = false;
        jsonTextAreas[`comp_list_${data.id}`].setValue(data.value);
        jsonTextAreas[`comp_list_${data.id}`].needEmitChanges = true;
    }
});

socket.on('component_changes_line_callback', (data) => {
    if(typeof jsonTextAreas[`comp_list_${data.id}`] !== 'undefined'){
        jsonTextAreas[`comp_list_${data.id}`].needEmitChanges = false;
        jsonTextAreas[`comp_list_${data.id}`].setValue(data.value);
        jsonTextAreas[`comp_list_${data.id}`].needEmitChanges = true;
    }
    M.toast({html: `${data.name} has been updated via LINE`, classes: 'rounded'});
});

socket.on('line_changes_callback', (data) => {
    _get_line_data();
    M.toast({html: data.message, classes: 'rounded'});
});