$("form").submit(function(){
    $.ajax({
        url:$(this).attr("action"),
        data:$(this).serialize(),
        type:$(this).attr("method"),
        dataType: 'html',
        beforeSend: function() {
            $("#username").attr("disabled",true);
            $("#password").attr("disabled",true);
            $("#submit").attr("disabled",true);
        },
        complete:function() {
            $("#username").attr("disabled",false);
            $("#password").attr("disabled",false);
            $("#submit").attr("disabled",false);							
        },
        success:function(data) {
            data = JSON.parse(data);
            toastr.success(data.message);
            localStorage.setItem("x_auth_token", data.token);
            setTimeout(()=>{
                window.location.href = "/backend";
            }, 1000);
        },
        error: function(data){
            data = JSON.parse(data.responseText);
            toastr.error(data.message);
        }
    });
    return false;
});

$('.form-button a').click(() => {
    Swal.fire({
        title: 'Register for an Account',
        html:`
            If you want to register, please kindly send me an email by clicking 
            <a href="mailto:arcphysx@gmail.com">here</a>
            <br/><br/>
            Thank you :)
        `,
        width: 600,
        padding: '3em',
        background: 'repeating-linear-gradient(43deg, rgba(161,159,159, 0.15) 0px, rgba(161,159,159, 0.15) 1px,transparent 1px, transparent 41px),repeating-linear-gradient(133deg, rgba(161,159,159, 0.15) 0px, rgba(161,159,159, 0.15) 1px,transparent 1px, transparent 41px),linear-gradient(133deg, rgb(179,229,252),rgb(179,229,252))',
        backdrop: `
          #82b1ff66
          url("/images/nyan-cat.gif")
          left top
          no-repeat
        `,
        imageUrl: '/images/logo.webp',
        imageWidth: 128,
        imageHeight: 128,
        imageAlt: 'E Controller Logo',
    });
});

$('.btn-about').click(() => {
    Swal.fire({
        title: 'About This Project',
        html:`
            <strong>E-Controller</strong> is an <i>Embedded System Project</i> 
            <br/>
            created by <strong>Raymond A P</strong>.
            <br/><br/>
            This app is useful for automate your daily electronic device with microcontroller, moreover you can connect your LINE Account with this app.
            <br/><br/>
            <strong>&copy;${(new Date()).getFullYear()} Raymond A P</strong>
        `,
        width: 600,
        padding: '3em',
        background: 'repeating-linear-gradient(43deg, rgba(161,159,159, 0.15) 0px, rgba(161,159,159, 0.15) 1px,transparent 1px, transparent 41px),repeating-linear-gradient(133deg, rgba(161,159,159, 0.15) 0px, rgba(161,159,159, 0.15) 1px,transparent 1px, transparent 41px),linear-gradient(133deg, rgb(179,229,252),rgb(179,229,252))',
        backdrop: `
          #82b1ff66
          url("/images/nyan-cat.gif")
          left top
          no-repeat
        `,
        imageUrl: '/images/logo.webp',
        imageWidth: 128,
        imageHeight: 128,
        imageAlt: 'E Controller Logo',
    });
});