const INTERFACE = require('./LineExecutor');

class FollowEvent{
    constructor(req, events, io){
        this.interface = new INTERFACE(req, events, io);
    }

    async execute(){
        var lineAccount = await this.interface.saveLineAccountIfNotExist(null, true);
    }
}

module.exports = FollowEvent;