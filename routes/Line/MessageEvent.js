const INTERFACE = require('./LineExecutor');
var models = require('require-all')(__dirname + '/../../models');
var CommandProcessor = require('../../config/commandProcessor');

var HelpCommandController = require('../LineResponder/HelpCommand');
var LoginCommandController = require('../LineResponder/LoginCommand');
var LogoutCommandController = require('../LineResponder/LogoutCommand');
var ComponentValueCommandController = require('../LineResponder/ComponentValueCommand');
var ComponentStateCommandController = require('../LineResponder/ComponentStateCommand');
var DeviceListCommandController = require('../LineResponder/DeviceListCommand');

var lineAccount = null;
var lineGroup = null;

class MessageEvent{
    constructor(req, events, io){
        this.interface = new INTERFACE(req, events, io);
    }

    async execute(){
        switch(this.interface.source.type){

            case "user":
                return this.userSourceHandler();
            
            // case "group":
            //     return this.groupSourceHandler();

            // case "room":
            //     return this.roomSourceHandler();

            default:
                return ;
        }
    }

    async userSourceHandler(){
        lineAccount = await this.interface.saveLineAccountIfNotExist(null, true);
        if(lineAccount != null){
            await models.LineAccountMessage.create({
                id_line_account: lineAccount.id,
                id_inbound_dump: this.interface.request.indump.id,
                type: this.interface.events.message.type,
                message_id: this.interface.events.message.id,
                content: JSON.stringify(this.getMessageContent()),
            });
        }
        
        if(this.interface.events.message.type != 'text'){
            return;
        }

        this.userCommandHandler();
    }

    userCommandHandler(){
        var processor = new CommandProcessor();
        var digested = processor.digest(this.interface.events.message.text);

        if(digested.is_command == false) return;

        var executor = null;
        switch(digested.parameters.command){

            // Handle bot login request
            case 'help':
                executor = new HelpCommandController(this.interface.request, this.interface.events, digested, this.interface.io, lineAccount);
                break;

            // Handle bot login request
            case 'login':
                executor = new LoginCommandController(this.interface.request, this.interface.events, digested, this.interface.io, lineAccount);
                break;

            // Handle bot login request
            case 'logout':
                executor = new LogoutCommandController(this.interface.request, this.interface.events, digested, this.interface.io, lineAccount);
                break;

            // Handle bot login request
            case 'chval':
                executor = new ComponentValueCommandController(this.interface.request, this.interface.events, digested, this.interface.io, lineAccount);
                break;

            // Handle bot login request
            case 'cstate':
                executor = new ComponentStateCommandController(this.interface.request, this.interface.events, digested, this.interface.io, lineAccount);
                break;

            case 'lsdev':
                executor = new DeviceListCommandController(this.interface.request, this.interface.events, digested, this.interface.io, lineAccount);
                break;
        }

        return executor.execute();
    }

    getMessageContent(){
        switch(this.interface.events.message.type){
            case 'text':
                if(typeof this.interface.events.message.emojis != 'undefined'){
                    return {
                        'message' : this.interface.events.message.text,
                        'emojis' : this.interface.events.message.emojis,
                    };
                }else{
                    return {
                        'message' : this.interface.events.message.text,
                    };
                }
                break;

            case 'image':
                return {
                    'contentProvider' : this.interface.events.message.contentProvider,
                };
                break;

            case 'video':
                return {
                    'duration' : this.interface.events.message.duration,
                    'contentProvider' : this.interface.events.message.contentProvider,
                };
                break;

            case 'audio':
                return {
                    'duration' : this.interface.events.message.duration,
                    'contentProvider' : this.interface.events.message.contentProvider,
                };
                break;
            
            case 'file':
                return {
                    'fileName' : this.interface.events.message.fileName,
                    'fileSize' : this.interface.events.message.fileSize,
                };
                break;
            
            case 'location':
                return {
                    'title' : this.interface.events.message.title,
                    'address' : this.interface.events.message.address,
                    'latitude' : this.interface.events.message.latitude,
                    'longitude' : this.interface.events.message.longitude,
                };
                break;

            case 'sticker':
                return {
                    'packageId' : this.interface.events.message.packageId,
                    'stickerId' : this.interface.events.message.stickerId,
                    'stickerResourceType' : this.interface.events.message.stickerResourceType,
                };
                break;
            
            default:
                return {};
        }
    }
}

module.exports = MessageEvent;