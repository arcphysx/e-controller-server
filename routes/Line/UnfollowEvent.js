const INTERFACE = require('./LineExecutor');

class UnfollowEvent{
    constructor(req, events, io){
        this.interface = new INTERFACE(req, events, io);
    }

    async execute(){
        var lineAccount = this.interface.saveLineAccountIfNotExist(null, true);
        if(lineAccount != null){
            lineAccount.id_user = 0;
            await lineAccount.save();
        }
    }
}

module.exports = UnfollowEvent;