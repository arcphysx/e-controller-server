var models = require('require-all')(__dirname + '/../../models');

class LineExecutor{
    constructor(req, events, io){
        this.request = req;
        this.events = events;
        this.source = events.source;
        this.io = io;
    }

    async saveLineAccountIfNotExist(lineUserId = "", returnObj = false){
        var account = null;
        if(lineUserId == "" || lineUserId == null){
            var lineAcc = await models.LineAccount.findOne({
                where:{
                    line_user_id: this.source.userId
                }
            });
            if(lineAcc == null){
                models.LineAccount.create({
                    line_user_id: this.source.userId,
                }).then((acc) => {
                    return (returnObj == false ? true : acc);
                }).catch((err) => {
                    return (returnObj == false ? false : null);
                });
            }else{
                account = lineAcc;
            }
        }else{
            var lineAcc = await models.LineAccount.findOne({
                where:{
                    line_user_id: lineUserId
                }
            });
            if(lineAcc == null){
                models.LineAccount.create({
                    line_user_id: lineUserId,
                }).then((acc) => {
                    return (returnObj == false ? true : acc);
                }).catch((err) => {
                    return (returnObj == false ? false : null);
                });
            }else{
                account = lineAcc;
            }
        }
        return (returnObj == false ? false : account)
    }
}

module.exports = LineExecutor;