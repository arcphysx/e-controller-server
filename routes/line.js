var express = require('express');
var router = express.Router();
var models = require('require-all')(__dirname + '/../models');
const SHA256 = require("crypto-js/sha256")
const {
  check,
  validationResult,
  body
} = require('express-validator');
const bcrypt = require('bcrypt');
const crypto = require("crypto");
const jwt = require('jsonwebtoken');

var FollowEventHandler = require('./Line/FollowEvent');
var UnfollowEventHandler = require('./Line/UnfollowEvent');
var MessageEventHandler = require('./Line/MessageEvent');

var mustBeAuthenticatedMiddleware = require("../middleware/mustBeAuthenticated");

module.exports = function(io) {

    router.post('/receiver', async function(req, res, next) {
        var events = req.body.events;

        if(events.length <=0) return res.status(200).json({ "message": "OK" });
        events = events[0];
        
        if(events.mode != 'active') return res.status(200).json({ "message": "OK" });

        var executor = null;
        switch(events.type){
            case "message":
                executor = new MessageEventHandler(req, events, io);
                break;
            
            case "follow":
                executor = new FollowEventHandler(req, events, io);
                break;

            case "unfollow":
                executor = new UnfollowEventHandler(req, events, io);
                break;

            default:
                return res.status(200).json({ "message": "OK" });
        }

        executor.execute();
        return res.status(200).json({ "message": "OK" });
    });

    router.use(mustBeAuthenticatedMiddleware);
    router.get('/connected', async function(req, res, next) {
        console.log('test');
        let lineAccount = await models.LineAccount.findOne({
            where: {
                id_user: req.auth.id
            }
        });

        return res.status(200).json({
            "message": "OK",
            "data": lineAccount,
        });
    });

    return router;
}