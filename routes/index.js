var express = require('express');
var router = express.Router();
var models = require('require-all')(__dirname + '/../models');
const {
  check,
  validationResult,
  body
} = require('express-validator');
const bcrypt = require('bcrypt');
const crypto = require("crypto");
const jwt = require('jsonwebtoken');

/* GET home page. */
router.get('/', function(req, res, next) {
  return res.redirect('/login');
});

router.get('/logout', function(req, res, next) {
  res.render('logout');
});

router.get('/login', function(req, res, next) {
  res.render('login');
});

router.post('/login', [
  // validate email
  body('username').isAscii(),

  // validate password
  body('password').isAscii()
], async function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      "message": "Some data contain errors",
      "errors": errors.array()
    });
  }

  let user = await models.User.findOne({
    where: {
      username: req.body.username
    }
  });

  if(user == null){
    return res.status(422).json({
      "message": "Credentials doesn't match in our record"
    });
  }

  // Start the authentication process
  if(!bcrypt.compareSync(req.body.password, user.password)){
    return res.status(422).json({
      "message": "Credentials doesn't match in our record"
    });
  }

  user.last_login = new Date();
  await user.save();
  
  var token = jwt.sign({
    "id": user.id,
    "username": user.username,
    "name": user.name,
    "last_login": user.last_login,
  }, process.env.APP_KEY);
  res.status(201).json({
    "message": "Welcome " + user.name,
    "token": token
  });

});

module.exports = router;
