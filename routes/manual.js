var express = require('express');
var router = express.Router();
const fs = require('fs');
const path = require('path');
var md = require('markdown-it')({
  html:         true,        // Enable HTML tags in source
  xhtmlOut:     false,        // Use '/' to close single tags (<br />).
                              // This is only for full CommonMark compatibility.
  breaks:       false,        // Convert '\n' in paragraphs into <br>
  langPrefix:   'language-',  // CSS language prefix for fenced blocks. Can be
                              // useful for external highlighters.
  linkify:      true,        // Autoconvert URL-like text to links
 
  // Enable some language-neutral replacement + quotes beautification
  typographer:  true,
 
  // Double + single quotes replacement pairs, when typographer enabled,
  // and smartquotes on. Could be either a String or an Array.
  //
  // For example, you can use '«»„“' for Russian, '„“‚‘' for German,
  // and ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'] for French (including nbsp).
  quotes: '“”‘’',
 
  // Highlighter function. Should return escaped HTML,
  // or '' if the source string is not changed and should be escaped externally.
  // If result starts with <pre... internal wrapper is skipped.
  highlight: function (/*str, lang*/) { return ''; }
});

const baseDir = path.join(__dirname, '../manual');

var readFiles = (filename) => {
  return new Promise((resolve, reject) => {
      fs.readFile(path.join(baseDir, filename), 'utf-8', function (err, content) {
          if (err) {
              reject( err );
          }
          resolve( content );
      });
  });
}

/* GET home page. */
router.get('/', function(req, res, next) {
  return res.render('manual');
});

router.get('/:name', function(req, res, next) {
  readFiles(req.params.name + ".md").then((data) => {
    var render = md.render(data);
    return res.render('manual', { 
      renderedMd : render, 
      crumbs: [
        { link: "/manual", name: "Manual" },
        { link: `/manual/${req.params.name}`, name: `${req.params.name}` },
      ]
    });
  }).catch((err) => {
    res.status(500).render('error', { message: "Oops... something went wrong", error:{} });
  });
});

module.exports = router;
