const INTERFACE = require('./HttpResponder');
var models = require('require-all')(__dirname + '/../../models');
const SHA256 = require("crypto-js/sha256");

class LogoutCommand{
    constructor(req, events, command, io, user, group){
        this.interface = new INTERFACE(req, events, command, io, user, group);
    }

    async execute(){
        await this.validateCommandParam();
    }

    async validateCommandParam(){
        if(
            this.interface.command.parameters.param.length > 0){
            return this.interface.sendInvalidCommandResponse();
        }

        return await this.executeCommand();
    }

    async executeCommand(){

        if(this.interface.user.id_user == null || this.interface.user.id_user == 0){
            return this.interface.pushTextMessage([
                {
                    type: "text",
                    text: `Maaf, saat ini kamu belum login\nSilahkan login terlebih dahulu`
                }
            ]);
        }

        var userId = this.interface.user.id_user;

        this.interface.user.id_user = 0;
        await this.interface.user.save();
        await this.interface.user.reload();

        this.interface.io.in(SHA256(userId + process.env.APP_KEY).toString()).emit('line_changes_callback', {"message" : "LINE Account has been logged out"});

        return this.interface.pushTextMessage([
            {
                "type" : "text",
                "text" : `Sampai jumpa ${this.interface.user.display_name}!
                \nAkun Anda telah berhasil logout`,
            }
        ]);
    }
}

module.exports = LogoutCommand;