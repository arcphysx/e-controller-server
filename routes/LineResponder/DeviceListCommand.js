const INTERFACE = require('./HttpResponder');
var models = require('require-all')(__dirname + '/../../models');
const { Op } = require("sequelize");
const Sequelize = require('sequelize');
const SHA256 = require("crypto-js/sha256");
const jsonBeautify = require("json-beautify");

class DeviceListCommand{
    constructor(req, events, command, io, user, group){
        this.interface = new INTERFACE(req, events, command, io, user, group);
    }

    async execute(){
        await this.validateCommandParam();
    }

    async validateCommandParam(){

        for (let i = 0; i < this.interface.command.parameters.param.length; i++) {
            var param = this.interface.command.parameters.param[i];
            if(typeof param['-'] != 'undefined'){
                return this.interface.sendInvalidCommandResponse();
            }
        }

        return await this.executeCommand();
    }

    async executeCommand(){
        var param = this.interface.command.parameters.param;

        if(this.interface.user.id_user == null || this.interface.user.id_user == 0){
            return this.interface.pushTextMessage([
                {
                    type: "text",
                    text: `Maaf, saat ini kamu belum login\nSilahkan login terlebih dahulu`
                }
            ]);
        }

        var user = await models.User.findByPk(this.interface.user.id_user);
        
        var devices = [];
        var output = "";
        var flag = false;

        if(param.length > 0){
            for (let i = 0; i < param.length; i++) {
                if(Object.keys(param[i])[0] == 'a'){
                    flag = true;
                    devices = await models.Device.findAll({
                        where:{
                            id_user: user.id
                        },
                        include: {
                            model: models.Component,
                            as: "Component"
                        }
                    });
                    output += `Daftar device milik '${user.name}':`;
                    
                    for (let i = 0; i < devices.length; i++) {
                        output += `\n${i+1}. ${devices[i].name} [${(devices[i].socket_uid == null || devices[i].socket_uid == "" ? "Offline" : "Online")}]`;
                        for (let j = 0; j < devices[i].Component.length; j++) {
                            var component = devices[i].Component[j];
                            output += `\n  > ${component.name}`;
                        }
                    }
                    break;
                }else if(Object.keys(param[i])[0] == 'f'){
                    flag = true;
                    devices = await models.Device.findAll({
                        where:[
                            Sequelize.where(
                                Sequelize.fn('LOWER', Sequelize.col('Device.name')),
                                {
                                    [Op.like]: '%' + param[i]['f'] + '%'
                                }
                            ),
                            {
                                id_user: user.id
                            }
                        ],
                        include: {
                            model: models.Component,
                            as: "Component"
                        }
                    });

                    output += `Daftar device milik '${user.name}':`;
                    
                    for (let i = 0; i < devices.length; i++) {
                        output += `\n${i+1}. ${devices[i].name} [${(devices[i].socket_uid == null || devices[i].socket_uid == "" ? "Offline" : "Online")}]`;
                        for (let j = 0; j < devices[i].Component.length; j++) {
                            var component = devices[i].Component[j];
                            output += `\n\t > ${component.name}`;
                        }
                    }
                    break;
                }
            }
        }else{
            flag = true;
            devices = await models.Device.findAll({
                where:{
                    id_user: user.id
                },
            });

            output += `Daftar device milik '${user.name}':`;
                    
            for (let i = 0; i < devices.length; i++) {
                output += `\n${i+1}. ${devices[i].name} [${(devices[i].socket_uid == null || devices[i].socket_uid == "" ? "Offline" : "Online")}]`;
            }
        }

        if(flag == true){
            return this.interface.pushTextMessage([
                {
                    "type" : "text",
                    "text" : output,
                }
            ]);
        }else{
            return this.interface.pushTextMessage([
                {
                    "type" : "text",
                    "text" : "Parameter perintah tidak sesuai",
                }
            ]);
        }
    }
}

module.exports = DeviceListCommand;