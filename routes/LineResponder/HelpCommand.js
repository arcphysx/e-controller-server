const INTERFACE = require('./HttpResponder');
var models = require('require-all')(__dirname + '/../../models');

class HelpCommand{
    constructor(req, events, command, io, user, group){
        this.interface = new INTERFACE(req, events, command, io, user, group);
    }

    async execute(){
        await this.validateCommandParam();
    }

    async validateCommandParam(){
        if(
            this.interface.command.parameters.param.length > 0){
            return this.interface.sendInvalidCommandResponse();
        }

        return await this.executeCommand();
    }

    async executeCommand(){

        return this.interface.pushTextMessage([
            {
                "type" : "text",
                "text" : `Untuk bantuan lebih lanjut dapat dilihat di https://micro.cium.in/manual`,
            }
        ]);
    }
}

module.exports = HelpCommand;