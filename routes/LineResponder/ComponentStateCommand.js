const INTERFACE = require('./HttpResponder');
var models = require('require-all')(__dirname + '/../../models');
const { Op } = require("sequelize");
const Sequelize = require('sequelize');
const SHA256 = require("crypto-js/sha256");
const jsonBeautify = require("json-beautify");

class ComponentStateCommand{
    constructor(req, events, command, io, user, group){
        this.interface = new INTERFACE(req, events, command, io, user, group);
    }

    async execute(){
        await this.validateCommandParam();
    }

    async validateCommandParam(){
        if(
            this.interface.command.parameters.param.length < 1){
            return this.interface.sendInvalidCommandResponse();
        }

        for (let i = 0; i < this.interface.command.parameters.param.length; i++) {
            var param = this.interface.command.parameters.param[i];
            if(i > 0 && typeof param['-'] != 'undefined'){
                return this.interface.sendInvalidCommandResponse();
            }
        }

        return await this.executeCommand();
    }

    async executeCommand(){
        var param = this.interface.command.parameters.param;

        if(this.interface.user.id_user == null || this.interface.user.id_user == 0){
            return this.interface.pushTextMessage([
                {
                    type: "text",
                    text: `Maaf, saat ini kamu belum login\nSilahkan login terlebih dahulu`
                }
            ]);
        }

        var user = await models.User.findByPk(this.interface.user.id_user);
        
        var component = await models.Component.findAll({
            where:[
                Sequelize.where(
                    Sequelize.fn('LOWER', Sequelize.col('Component.name')),
                    {
                        [Op.like]: '%' + param[0]['-'] + '%'
                    }
                )
            ],
            include: {
                model: models.Device,
                as: "Device",
                attributes: ['id', 'id_user'],
                where:{
                    id_user: user.id
                }
            }
        });

        if(component.length > 1){
            return this.interface.pushTextMessage([
                {
                    type: "text",
                    text: `Ada lebih dari 1 komponen dengan nama serupa\nHarap berikan nama lebih detail`
                }
            ]);
        }
        
        component = component[0];
        var value = JSON.parse(component.value);
        var output = "";

        if(param.length > 1){
            output += `Nilai komponen '${component.name}':`;
            for (let i = 1; i < param.length; i++) {
                if(typeof value[Object.keys(param[i])[0]] == 'undefined'){
                    return this.interface.pushTextMessage([
                        {
                            type: "text",
                            text: `Nilai '${Object.keys(param[i])[0]}' pada komponen '${component.name}' tidak ditemukan`
                        }
                    ]);
                }else{
                    output += `\n${Object.keys(param[i])[0]}= ${value[Object.keys(param[i])[0]]}`;
                }
            }
        }else{
            output += `Nilai komponen '${component.name}':`;
            for (let i = 0; i < Object.keys(value).length; i++) {
                output += `\n${Object.keys(value)[i]}= ${value[Object.keys(value)[i]]}`;
            }
        }

        return this.interface.pushTextMessage([
            {
                "type" : "text",
                "text" : output,
            }
        ]);
    }
}

module.exports = ComponentStateCommand;