var models = require('require-all')(__dirname + '/../../models');
const axios = require('axios');

var REPLY_BASE_URL = "https://api.line.me/v2/bot/message/reply";
var API_KEY = "";

class HttpResponder{
    constructor(req, events, command, io, user = null, group = null){
        API_KEY = process.env.LINE_API_KEY;
        this.request = req;
        this.events = events;
        this.message = events.message;
        this.command = command;
        this.user = user;
        this.group = group;
        this.io = io;
    }

    pushTextMessage(messages){
        var config = {
            headers: {
                'Authorization' : 'Bearer ' + API_KEY,
                'Content-Type' : 'application/json',
            }
        };
        var body = {
            'replyToken' : this.events.replyToken,
            'messages' : messages,
        };

        axios.post(REPLY_BASE_URL, body, config)
        .then((response) => {
            console.log(response);
        }).catch((error) => {
            console.log(error);
        });
    }

    sendInvalidCommandResponse(){
        this.pushTextMessage([{
            "type" : "text",
            "text" : "Maaf Perintah Anda tidak dikenali",
        }]);
    }
}

module.exports = HttpResponder;