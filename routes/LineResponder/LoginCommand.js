const INTERFACE = require('./HttpResponder');
var models = require('require-all')(__dirname + '/../../models');
const SHA256 = require("crypto-js/sha256");

var username;

class LoginCommand{
    constructor(req, events, command, io, user, group){
        this.interface = new INTERFACE(req, events, command, io, user, group);
    }

    async execute(){
        await this.validateCommandParam();
    }

    async validateCommandParam(){
        if(
            this.interface.command.parameters.param.length > 1 ||
            this.interface.command.parameters.param.length < 1
        ){
            return this.interface.sendInvalidCommandResponse();
        }

        // This command should get plain api key text
        if(typeof this.interface.command.parameters.param[0]['-'] == 'undefined'){
            return this.interface.sendInvalidCommandResponse();
        }

        username = this.interface.command.parameters.param[0]['-'];
        return await this.executeCommand();
    }

    async executeCommand(){
        var user = await models.User.findOne({
            where:{
                username: username
            },
            include: {
                model: models.LineAccount,
                as: "LineAccount",
            }
        });

        if(user == null){
            return this.interface.pushTextMessage([
                {
                    type: "text",
                    text: `"User dengan username '${username}' tidak ditemukan :("`
                }
            ]);
        }

        if(user.LineAccount.length > 0){
            return this.interface.pushTextMessage([
                {
                    type: "text",
                    text: `Maaf, user dengan username '${username}' telah terhubung dengan LINE Account\nSilahkan logout terlebih dahulu`
                }
            ]);
        }
        console.log("AAA");
        if(this.interface.user.id_user != null && this.interface.user.id_user != 0){
            return this.interface.pushTextMessage([
                {
                    type: "text",
                    text: `Maaf, saat ini kamu telah login\nSilahkan logout terlebih dahulu`
                }
            ]);
        }

        
        await this.interface.user.syncDataWithLineServer();
        this.interface.user.id_user = user.id;
        await this.interface.user.save();
        await this.interface.user.reload();

        this.interface.io.in(SHA256(user.id + process.env.APP_KEY).toString()).emit('line_changes_callback', {"message" : "LINE Account has been logged in"});
        
        return this.interface.pushTextMessage([
            {
                "type" : "text",
                "text" : `Hi ${user.name}!
                \nAkun Anda telah berhasil terhubung`,
            }
        ]);
    }
}

module.exports = LoginCommand;