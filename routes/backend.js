var express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken');
const SHA256 = require("crypto-js/sha256");
var models = require('require-all')(__dirname + '/../models');
const { Op } = require("sequelize");
const jsonBeautify = require("json-beautify");

var _user = {};
var _device = {};

var mustBeAuthenticatedMiddleware = require("../middleware/mustBeAuthenticated");

// Reset All Device State
models.Device.findAll({
    where: {
      [Op.not]: [{socket_uid: null}] 
    }
}).then(async (devices)=>{
  for(let i=0; i<devices.length; i++){
    devices[i].socket_uid = null;
    await devices[i].save();
  }
});

module.exports = function(io) {

  // Socket Authentication Middleware
  io.use(async (socket, next) => {
    var handshakeData = socket.request;
    
    if(handshakeData._query['type'] == "panel"){
      jwt.verify(handshakeData._query['authorization'], process.env.APP_KEY, async function(err, decoded) {
        if(err){
          let err  = new Error('Authentication error');
          err.data = { code: "401",  message: "Unauthorized"};
          return next(err);
        }
        _user = decoded;
        return next();
      });
    }else if(handshakeData._query['type'] == "device"){
      let device = await models.Device.findOne({
        where: {
          key: handshakeData._query['authorization']
        }
      });

      if(device == null){
        let err  = new Error('Authentication error');
        err.data = { code: "401",  message: "Unauthorized"};
        return next(err);
      }

      if(device.socket_uid != null && device.socket_uid != ''){
        socket.to(SHA256(device.id_user + process.env.APP_KEY).toString()).emit('device_leave', device);
      }

      device.socket_uid = socket.id;
      await device.save();
      
      _device = device;
      return next();
    }else{
      let err  = new Error('Authentication error');
      err.data = { code: "401",  message: "Unauthorized"};
      return next(err);
    }

  });

  io.on('connection', async (socket) => {

    if(socket.request._query['type'] == "device"){
      socket.join(SHA256(_device.id_user + process.env.APP_KEY).toString());
      socket.to(SHA256(_device.id_user + process.env.APP_KEY).toString()).emit('device_join', _device);
      
    }else if(socket.request._query['type'] == "panel"){
      socket.join(SHA256(_user.id + process.env.APP_KEY).toString());
    }

    socket.on('connect', (data) => {
      
    });

    socket.on('request_component_update_all', async (data) => {
      let device = await models.Device.findOne({
        where: {
          key: data.key
        },
        include: {
          model: models.Component,
          as: "Component",
          attributes: ['id', 'value', 'type'],
        }
      });

      let components = device.Component;
      let values = [];
      for(i in components){
        obj = JSON.parse(components[i].dataValues.value);
        values.push({
          "id": components[i].dataValues.id,
          "type": components[i].dataValues.type.toLowerCase(),
          "value": obj
        });
      }
      console.log(values);
      socket.emit('component_update_all', values);
    });

    socket.on('request_component_changes', async (data) => {
      // jsonBeautify
      let component = await models.Component.findOne({
        where:{
          id: data.id
        },
        include: {
          model: models.Device,
          as: "Device",
        }
      });

      var flagSave = false;
      var value = JSON.parse(component.value);
      if(component.type == "sensor_temperature"){
        value.humidity = data.humidity;
        value.temperature = data.temperature;
        value = jsonBeautify(value, null, 2, 25);
        
        component.value = value;
        await component.save();
        flagSave = true;
      }
      
      if(flagSave == true){
        socket.to(SHA256(component.Device.id_user + process.env.APP_KEY).toString()).emit('component_changes_callback', component);
      }
      
    });

    socket.on('component_update', async (data) => {
      if(typeof data.id != 'undefined'){
        let component = await models.Component.findOne({
          where:{
            id: data.id
          },
          include: {
            model: models.Device,
            as: "Device",
          }
        });

        if(component !== null){
          let isSaved = false;
          if(typeof data.name != 'undefined'){
            component.name = data.name;
            await component.save();
            isSaved = true;
          }

          if(typeof data.type != 'undefined'){
            component.type = data.type;
            await component.save();
            isSaved = true;
          }

          if(typeof data.value != 'undefined'){
            try {
              JSON.parse(data.value);
              component.value = data.value;
              await component.save();
              isSaved = true;
            } catch (e) {
                
            }
          }
          
          if(isSaved){
            obj = JSON.parse(component.value);
            value = {
              "id": component.id,
              "type": component.type.toLowerCase(),
              "value": obj
            };
            console.log(value);
            socket.to(SHA256(component.Device.id_user + process.env.APP_KEY).toString()).emit('component_update_one', value);
          }
        }

      }
    });

    socket.on('component_add', async (data) => {
      models.Component.create({
        id_device: data.device_id,
        name: "Untitled Component",
        type: "led",
        value: "{\n\t\"pin\":[]\n}"
      }).then(async (comp) => {
        let device = await models.Device.findByPk(data.device_id);
        socket.emit('component_add_callback', {"code": 201, "message": "OK", "data" : { device_key: device.key }});
      }).catch((err) => {
        socket.emit('component_add_callback', {"code": 500, "error" : err});
      });
    });

    socket.on('component_delete', async (data) => {
      let component = await models.Component.findOne({
        where: {
          id: data.comp_id
        },
        include: {
          model: models.Device,
          as: "Device",
        }
      });
      await component.destroy();
      socket.emit('component_delete_callback', {"code": 201, "message": "OK", "data" : { device_key: component.Device.key }});
    });

    socket.on('device_add', async (data) => {
      models.Device.create({
        id_user: data.user_id,
        name: data.name,
        key: SHA256(data.user_id + (new Date()).toString() + process.env.APP_KEY).toString().substr(0, 32),
      }).then(async (dev) => {
        socket.emit('device_add_callback', {"code": 201, "message": "OK"});
      }).catch((err) => {
        socket.emit('device_add_callback', {"code": 500, "error" : err});
      });
    });

    socket.on('device_edit', async (data) => {
      console.log(data);
      let device = await models.Device.findOne({
        where: {
          id: data.device_id,
          id_user: data.user_id
        }
      });
      device.name = data.name;
      await device.save();
      socket.emit('device_edit_callback', {"code": 201, "message": "OK", "data": device});
    });

    socket.on('device_delete', async (data) => {
      let device = await models.Device.findOne({
        where: {
          id: data.device_id,
          id_user: data.user_id
        }
      });
      await device.destroy();
      socket.emit('device_delete_callback', {"code": 201, "message": "OK"});
    });

    socket.on('line_logout', async (data) => {
      let user = await models.User.findOne({
        where: {
          id: data.user_id,
        },
        include: {
          model: models.LineAccount,
          as: "LineAccount",
        }
      });

      if(user.LineAccount.length > 0){
        for (let i = 0; i < user.LineAccount.length; i++) {
          var line = user.LineAccount[i];
          line.id_user = 0;
          await line.save();
        }
      }
      
      socket.emit('line_changes_callback', {"message" : "LINE Account has been logged out"});
    });

    socket.once('disconnect', async function () {
      let device = await models.Device.findOne({
        where: {
          socket_uid: socket.id
        }
      });
      console.log(socket.id);
      if(device != null){
        device.socket_uid = null;
        await device.save();
      }
      socket.to(SHA256(device.id_user + process.env.APP_KEY).toString()).emit('device_leave', device);
    });

  });

  router.get('/', function(req, res, next) {
    res.render('home', {req:req});
  });

  // Route below requires authenticated user
  router.use(mustBeAuthenticatedMiddleware);

  router.post('/check', function(req, res, next) {
    return res.status(200).json({
      "message": "OK",
    });
  });

  router.get('/devices', async function(req, res, next) {
    let devices = await models.Device.findAll({
      where: {
        id_user: req.auth.id
      }
    });

    return res.render('render/device_list', {devices : devices});

  });

  router.get('/devices/:id/components', async function(req, res, next) {
    let components = await models.Component.findAll({
      where: {
        id_device: req.params.id
      }
    });

    return res.render('render/component_list', {components : components});

  });

  return router;
}
