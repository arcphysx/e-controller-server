var models = require('require-all')(__dirname + '/../models');
const jwt = require('jsonwebtoken');

module.exports = async (req, res, next) => {
    if(typeof req.headers.authorization === 'undefined' || req.headers.authorization == null || req.headers.authorization == ''){
        res.status(401).json({
            "message": "Unauthorized"
        });
    }

    jwt.verify(req.headers.authorization, process.env.APP_KEY, async function(err, decoded) {
        if(err){
            res.status(401).json({
                "message": "Unauthorized",
                "err": err
            });
            return;
        }
        req.auth = await models.User.findByPk(decoded.id);
        return next();
    });

}