var models = require('require-all')(__dirname + '/../models');

module.exports = (req, res, next) => {
    models.InboundDump.create({
        'ip_address': req.ip,
        'request_header': JSON.stringify(req.headers),
        'request_body': JSON.stringify(req.body),
        'request_param': JSON.stringify(req.params),
    }).then((inbound) => {
        req.indump = inbound;
        next();
    }).catch(err => {
        res.status(500).json({
          "message": "Oops... something went wrong",
          "errors": err
        });
    });
}