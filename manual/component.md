# **Components**
---
- **[Introduction](#intro)**
- **[Available Components](#cmd)**
    - [Light Emitting Diode (LED)](#comp-led)
    - [RGB LED](#comp-led-rgb)
    - [Temperature and Humidity Sensor](#comp-sensor-temp)
---
<a name="intro" href="#intro">
    <h2>Introduction</h2>
</a>
Component is the electronic device that are connected with your device. A device could have more than one component in it. The list of all supported component is written in this section.

<a id="cmd" name="cmd" href="#cmd">
    <h2>Available Components</h2>
</a>
To learn about E-Controller's supported component, let's look at a complete example of LINE Command on this section.

<a id="comp-led" name="comp-led" href="#comp-led">
    <h4>Light Emitting Diode (LED)</h4>
</a>

A light-emitting diode (LED) is a semiconductor light source that emits light when current flows through it. Electrons in the semiconductor recombine with electron holes, releasing energy in the form of photons. The color of the light (corresponding to the energy of the photons) is determined by the energy required for electrons to cross the band gap of the semiconductor. White light is obtained by using multiple semiconductors or a layer of light-emitting phosphor on the semiconductor device.

**Component Default Attribute**
Each component has a default attribute which define its state at a time, the default attribute will be described in this section
```json
{
  "pin": [ 12 ],
  "state": 0
}
```
* `pin` - is a list of pin where the led is connected (Anode)
* `state` - is the state of the led `[1 = HIGH/ON | 0 = LOW/OFF]`


<a id="comp-led-rgb" name="comp-led-rgb" href="#comp-led-rgb">
    <h4>RGB LED</h4>
</a>

RGB LED means red, blue and green LEDs. RGB LED products combine these three colors to produce over 16 million hues of light. Note that not all colors are possible. Some colors are “outside” the triangle formed by the RGB LEDs. Also, pigment colors such as brown or pink are difficult, or impossible, to achieve.

**Component Default Attribute**
Each component has a default attribute which define its state at a time, the default attribute will be described in this section
```json
{
  "pin": [ 25, 26, 27 ],
  "color": [ 0, 0, 0 ]
}
```
* `pin` - is a list of pin where the led is connected
* `color` - is the color of the rgb led `[red, green, brue]` with each color range `0-255`


<a id="comp-sensor-temp" name="comp-sensor-temp" href="#comp-sensor-temp">
    <h4>Temperature and Humidity Sensor</h4>
</a>

A temperature sensor is a device, typically, a thermocouple or RTD, that provides for temperature measurement through an electrical signal. A thermocouple (T/C) is made from two dissimilar metals that generate electrical voltage in direct proportion to changes in temperature.

**Component Default Attribute**
Each component has a default attribute which define its state at a time, the default attribute will be described in this section
```json
{
  "pin": [ 32 ],
  "interval": 200,
  "humidity": 70,
  "temperature": 31
}
```
* `pin` - is a list of pin where the led is connected
* `interval` - is the broadcast interval of the sensor
* `humidity` - is the humidity value of the current environment
* `temperature` - is the temperature value of the current environment
