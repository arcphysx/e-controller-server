# **LINE Command**
---
- **[Introduction](#intro)**
- **[Available Commands](#cmd)**
    - [Connect Your LINE Account](#cmd-login)
    - [Remove Your LINE Account](#cmd-logout)
    - [Get Device List](#cmd-lsdev)
    - [Get Component State](#cmd-cstate)
    - [Change Component State](#cmd-chval)
---
<a name="intro" href="#intro">
    <h2>Introduction</h2>
</a>
E-Controller provides some command that can be used for remoting your device, and event change it's state. The list of all available LINE commands is written in this section.

<a id="cmd" name="cmd" href="#cmd">
    <h2>Available Commands</h2>
</a>
To learn about E-Controller's powerful LINE Command features, let's look at a complete example of LINE Command on this section.

<a id="cmd-login" name="cmd-login" href="#cmd-login">
    <h4>Connect Your LINE Account</h4>
</a>

To use all other LINE command, make sure you connect your LINE Account with E-Controller by using this command:
```bash
!login <your_username>
```

For example:
```bash
!login arcphysx
```


<a id="cmd-logout" name="cmd-logout" href="#cmd-logout">
    <h4>Remove Your LINE Account</h4>
</a>

This command is used for unlinking your E-Controller Account and your LINE Account. You will not be able to remote your E-Controller device, unless you re-connect your LINE Account. The command is stated as below:
```bash
!logout
```

For example:
```bash
!logout
```


<a id="cmd-lsdev" name="cmd-lsdev" href="#cmd-lsdev">
    <h4>Get Device List</h4>
</a>

This command is used for retrieving all your device including its availability status. You can also get a list of all components that connected with your device. The command is stated as below:
```bash
!lsdev [-a] [-f <device_name>]
```

For example:
```bash
!lsdev -a
```
```bash
!lsdev -f "NodeMCU 32S"
```


<a id="cmd-cstate" name="cmd-cstate" href="#cmd-cstate">
    <h4>Get Component State</h4>
</a>

This command is used for getting state of your connected component state. The command is stated as below:
```bash
!cstate <component_name>
```

For example:
```bash
!cstate Led
```
```bash
!cstate "Lampu LED RGB"
```


<a id="cmd-chval" name="cmd-chval" href="#cmd-chval">
    <h4>Change Component State</h4>
</a>

This command is used for changing state of your connected component state. You can change more than one component state at a time. The command is stated as below:
```bash
!chval <component_name> -<attribute_name> <attribute_value> ...
```

For example:
```bash
!chval Led -state 1
```
```bash
!chval "Lampu LED RGB" -color [67,120,78] -pin [25,26,27]
```
